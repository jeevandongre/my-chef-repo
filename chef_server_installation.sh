#this script will help to set up the chef server of the ubuntu 12.04 LTS AWS EC2 instance.
#run this script with sudo.
#make sure you have root permissions to run the script
!# /bin/bash

#Adds the opscode repo to the sources.list

echo "deb http://apt.opscode.com/ `lsb_release -cs`-0.10 main" | sudo tee /etc/apt/sources.list.d/opscode.list


#adding the keys 
sudo mkdir -p /etc/apt/trusted.gpg.d
gpg --keyserver keys.gnupg.net --recv-keys 83EF826A
gpg --export packages@opscode.com | sudo tee /etc/apt/trusted.gpg.d/opscode-keyring.gpg > /dev/null

#running an update 
sudo apt-get update && sudo apt-get install opscode-keyring # permanent upgradeable keyring
#installing chef,chef-server packages
sudo apt-get install chef chef-server
mkdir -p ~/.chef
sudo cp /etc/chef/validation.pem /etc/chef/webui.pem ~/.chef
sudo chown -R $USER ~/.chef

knife configure -i

 
