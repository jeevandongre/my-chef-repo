!# /bin/bash

#adds chef repo to the sources.list
echo "deb http://apt.opscode.com/ `lsb_release -cs`-0.10 main" | sudo tee /etc/apt/sources.list.d/opscode.list

#Add the GPG Key and Update Index
sudo mkdir -p /etc/apt/trusted.gpg.d
gpg --keyserver keys.gnupg.net --recv-keys 83EF826A
gpg --export packages@opscode.com | sudo tee /etc/apt/trusted.gpg.d/opscode-keyring.gpg > /dev/null

sudo apt-get update

sudo apt-get install opscode-keyring # permanent upgradeable keyring

sudo apt-get install chef

#echo "add your validation key here" >> /etc/chef/validation.pem

#sudo chef-client -N "node name"
